# python models file

# mvf imports
import pandas
# user imports
import pickle
from scipy import stats
from sklearn.linear_model import BayesianRidge
from sklearn.preprocessing import StandardScaler


class bayes_reg_unscaled:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = BayesianRidge()

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        self.model.fit(X, y)

    def validate(self):
        print(self.model.get_params())

    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred, std_err = self.model.predict(
            X,
            return_std=True
        )
        qs = list(set([q for qi in quantiles for q in qi]))
        zs = stats.zscore(qs)
        preds = pandas.DataFrame()
        preds['count'] = mean_pred
        preds['std_error'] = std_err
        for q, z in zip(qs, zs):
            preds[f'count_Q{q * 100:g}'] = mean_pred + (z * std_err)
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)


class bayes_reg_scaled:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = BayesianRidge()
        self.scaler = StandardScaler()

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        self.scaler.fit(X)
        self.model.fit(
            self.scaler.transform(X),
            y
        )

    def validate(self):
        print(self.model.get_params())

    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred, std_err = self.model.predict(
            self.scaler.transform(X),
            return_std=True
        )
        qs = list(set([q for qi in quantiles for q in qi]))
        zs = stats.zscore(qs)
        preds = pandas.DataFrame()
        preds['count'] = mean_pred
        preds['std_error'] = std_err
        for q, z in zip(qs, zs):
            preds[f'count_Q{q * 100:g}'] = mean_pred + (z * std_err)
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)
        with open(f'{path}_scaler', 'wb') as f:
            pickle.dump(self.scaler, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)
        with open(f'{path}_scaler', 'rb') as f:
            self.scaler = pickle.load(f)
