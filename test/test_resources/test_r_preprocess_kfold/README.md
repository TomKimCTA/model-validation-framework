This example project tests the

* R `preprocess_data` template
* `kfold` parameter on the `split_data` process

The project should run using `ploomber build`
