# python models file

# mvf imports
import pandas
# user imports
import pickle
from scipy import stats
from sklearn.linear_model import BayesianRidge

class py_bayes_reg:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = BayesianRidge()

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        self.model.fit(X, y)
    
    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred = self.model.predict(
            X
        )
        preds = pandas.DataFrame()
        preds['count'] = mean_pred
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)   
        