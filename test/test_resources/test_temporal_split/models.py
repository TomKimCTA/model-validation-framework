import pandas
import pickle

class test_kfold_py:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = None

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        pass

    def predict(self, X:pandas.DataFrame, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        preds = pandas.DataFrame(
            {
                'y1': X['X1'],
                'y2': 2 * X['X1']
            }
        )
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)