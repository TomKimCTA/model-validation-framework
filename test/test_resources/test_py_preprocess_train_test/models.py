# python models file

# mvf imports
import pandas
# user imports
import pickle
from scipy import stats
from sklearn.linear_model import BayesianRidge

class py_bayes_reg:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = BayesianRidge()

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        self.model.fit(X, y)
    
    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred, std_err = self.model.predict(
            X, 
            return_std=True
        )
        qs = list(set([q for qi in quantiles for q in qi]))
        zs = stats.zscore(qs)
        preds = pandas.DataFrame()
        preds['count'] = mean_pred
        preds['std_error'] = std_err
        for q, z in zip(qs, zs):
            preds[f'count_Q{q * 100:g}'] = mean_pred + (z * std_err)
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)   
        