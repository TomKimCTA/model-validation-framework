# Python models file

# MVF imports
import pandas
# User imports


class py_model1:
    '''
    Class implementing py_model1.
    '''

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        raise NotImplementedError

    def predict(self, X, quantiles=None):
        '''
        Predict target values (y) from new data.
        '''
        raise NotImplementedError

    def save(self, path):
        '''
        Save model.
        '''
        raise NotImplementedError

    def load(self, path):
        '''
        Load a model.
        '''
        raise NotImplementedError


class py_model2:
    '''
    Class implementing py_model2.
    '''

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        raise NotImplementedError

    def predict(self, X, quantiles=None):
        '''
        Predict target values (y) from new data.
        '''
        raise NotImplementedError

    def save(self, path):
        '''
        Save model.
        '''
        raise NotImplementedError

    def load(self, path):
        '''
        Load a model.
        '''
        raise NotImplementedError
