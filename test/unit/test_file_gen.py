from unittest.mock import patch
from mvf.template.file_gen import FileGenerator
import pytest
from testbook import testbook
import filecmp


@pytest.mark.parametrize(
    ('in_args', 'exp_note'),
    [
        (
            ('test/test_resources/test_data/outputs/preprocess_data.ipynb', 'Python'),
            'test/test_resources/test_data/preprocess_data_py.ipynb'
        ),
        (
            ('test/test_resources/test_data/outputs/preprocess_data.ipynb', 'R'),
            'test/test_resources/test_data/preprocess_data_r.ipynb'
        )
    ]
)
def test_gen_preprocess(in_args, exp_note):
    '''
    Test generation of preprocess template.
    '''
    FileGenerator().gen_preprocess(*in_args)

    with testbook(exp_note, execute=False) as exp_note:
        with testbook(in_args[0], execute=False) as gen_note:
            assert exp_note.kernel_name == gen_note.kernel_name
            assert len(exp_note.cells) == len(gen_note.cells)
            # Assert each cell source and type is equal
            for c1, c2 in zip(exp_note.cells, gen_note.cells):
                assert c1['source'] == c2['source']
                assert c1['cell_type'] == c2['cell_type']


@pytest.mark.parametrize(
    ('in_args', 'exp_file'),
    [
        (
            (['py_model'], 'test/test_resources/test_data/outputs/models.py'),
            'test/test_resources/test_data/models1.py'
        ),
        (
            (['py_model1', 'py_model2'],
             'test/test_resources/test_data/outputs/models.py'),
            'test/test_resources/test_data/models2.py'
        ),
    ]
)
def test_gen_models_py(in_args, exp_file):
    '''
    Test generation of Python models file.
    '''
    FileGenerator().gen_models_py(*in_args)

    assert filecmp.cmp(in_args[1], exp_file)


@patch('builtins.open')
def test_gen_models_py_no_models(mock_open):
    '''
    Tests behaviour when models param is empty list.
    '''
    FileGenerator().gen_models_py([])

    mock_open.assert_not_called()


@pytest.mark.parametrize(
    ('in_args', 'exp_file'),
    [
        (
            (['r_model'], 'test/test_resources/test_data/outputs/models.R'),
            'test/test_resources/test_data/models1.R'
        ),
        (
            (['r_model1', 'r_model2'], 'test/test_resources/test_data/outputs/models.R'),
            'test/test_resources/test_data/models2.R'
        ),
    ]
)
def test_gen_models_r(in_args, exp_file):
    '''
    Test generation of R models file.
    '''
    FileGenerator().gen_models_r(*in_args)

    assert filecmp.cmp(in_args[1], exp_file)


@patch('builtins.open')
def test_gen_models_r_no_models(mock_open):
    '''
    Tests behaviour when models param is empty list.
    '''
    FileGenerator().gen_models_r([])

    mock_open.assert_not_called()
