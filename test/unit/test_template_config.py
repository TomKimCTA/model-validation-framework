from mvf.template.config import ConfigBuilder
import os
import pytest
from unittest.mock import patch


@pytest.mark.parametrize(
    ('pth', 'ext_cfg', 'exp'),
    [
        (
            os.path.join(
                os.getcwd(),
                'test'
            ),
            None,
            {
                'cfg': {
                    'data': {
                        'source': 'path_to_your_source_code',
                        'lang': 'Python',
                        'split': 'train_test',
                        'test_size': 0.3,
                        'input_features': [],
                        'target_features': []
                    },
                    'models': [
                        {
                            'name': 'your_model_name',
                            'lang': 'Python'
                        },
                    ],
                },
                'mkdir_call': False,
                'open_call_count': 1
            }
        ),
        (
            os.path.join(
                os.getcwd(),
                'junk'
            ),
            None,
            {
                'cfg': {
                    'data': {
                        'source': 'path_to_your_source_code',
                        'lang': 'Python',
                        'split': 'train_test',
                        'test_size': 0.3,
                        'input_features': [],
                        'target_features': []
                    },
                    'models': [
                        {
                            'name': 'your_model_name',
                            'lang': 'Python'
                        },
                    ],
                },
                'mkdir_call': True,
                'open_call_count': 1
            }
        ),
        (
            os.path.join(
                os.getcwd(),
                'test',
                'test_resources',
                'test_kfold_end_to_end'
            ),
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'kfold',
                    'n_folds': 10,
                },
            },
            {
                'cfg': {
                    'data': {
                        'source': 'preprocess_data.ipynb',
                        'lang': 'Python',
                        'split': 'kfold',
                        'n_folds': 10,
                    },
                    'models': [
                        {
                            'name': 'your_model_name',
                            'lang': 'Python'
                        },
                    ],
                },
                'mkdir_call': False,
                'open_call_count': 2
            }
        ),
        (
            os.path.join(
                os.getcwd(),
                'test',
                'test_resources',
                'test_kfold_end_to_end'
            ),
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'kfold',
                    'n_folds': 10,
                },
                'output': {
                    'quantile_intervals': [[0.05, 0.95]]
                },
                'models': [
                    {
                        'name': 'my_first_model',
                        'lang': 'Python'
                    },
                    {
                        'name': 'my_second_model',
                        'lang': 'R',
                        'validation_step': True,
                    },
                ],
            },
            {
                'cfg': {
                    'data': {
                        'source': 'preprocess_data.ipynb',
                        'lang': 'Python',
                        'split': 'kfold',
                        'n_folds': 10,
                    },
                    'output': {
                        'quantile_intervals': [[0.05, 0.95]]
                    },
                    'models': [
                        {
                            'name': 'my_first_model',
                            'lang': 'Python'
                        },
                        {
                            'name': 'my_second_model',
                            'lang': 'R',
                            'validation_step': True,
                        },
                    ],
                },
                'mkdir_call': False,
                'open_call_count': 2
            }
        )
    ]
)
@patch('yaml.safe_dump')
@patch('builtins.open')
@patch('yaml.safe_load')
def test_config_builder(mock_load, mock_open, mock_dump, pth, ext_cfg, exp):
    '''
    Test logic for different states of pth.
    '''
    mock_load.return_value = ext_cfg

    config_builder = ConfigBuilder(pth)
    config_builder.write()

    assert config_builder.config == exp['cfg'], f'{config_builder.config}'
    assert mock_open.call_count == exp['open_call_count']
