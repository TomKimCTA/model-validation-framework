import pytest
import mvf.integration.process

#
# Tests for mvf.integration.process module
#


@pytest.mark.parametrize(
    ('process', 'func', 'kwargs'),
    [
        (
            'split_data',
            'split_data',
            {
                'product': {
                    'train_X_data': 'test/test_resources/test_data/train_X_data.feather',
                    'train_y_data': 'test/test_resources/test_data/train_y_data.feather',
                    'test_X_data': 'test/test_resources/test_data/test_X_data.feather',
                    'test_y_data': 'test/test_resources/test_data/test_y_data.feather',
                },
                'params': {
                    'split_type': 'train_test',
                    'test_size': 0.3,
                }
            }
        ),
        (
            'split_data',
            'split_data',
            {
                'product': {
                    'fold_1_X_data': 'test/test_resources/test_data/fold_1_X_data.feather',
                    'fold_1_y_data': 'test/test_resources/test_data/fold_1_y_data.feather',
                    'fold_2_X_data': 'test/test_resources/test_data/fold_2_X_data.feather',
                    'fold_2_y_data': 'test/test_resources/test_data/fold_2_y_data.feather',
                    'fold_3_X_data': 'test/test_resources/test_data/fold_3_X_data.feather',
                    'fold_3_y_data': 'test/test_resources/test_data/fold_3_y_data.feather',
                },
                'params': {
                    'split_type': 'k_fold',
                    'n_folds': 3
                }
            }
        ),
    ]
)
def test_integration_process_split_data_pass(process, func, kwargs):
    '''
    Check integration test passes with test data.
    '''
    process_module = getattr(mvf.integration.process, f'{process}')
    process_test = getattr(process_module, f'{func}')
    process_test(**kwargs)


@pytest.mark.parametrize(
    ('process', 'func', 'kwargs', 'exp_err'),
    [
        # invalid split type
        (
            'split_data',
            'split_data',
            {
                'product': {},
                'params': {
                    'split_type': 'another',
                }
            },
            NotImplementedError
        ),
    ]
)
def test_integration_process_split_data_fail(process, func, kwargs, exp_err):
    '''
    Check integration test fails in right circumstances.
    '''
    process_module = getattr(mvf.integration.process, f'{process}')
    process_test = getattr(process_module, f'{func}')
    with pytest.raises(exp_err):
        process_test(**kwargs)

