from unittest.mock import patch, MagicMock
import pandas
import pytest
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
import rpy2_r6.r6b as r6b
import time
from mvf.process.utils import ProcessTimer, convert_to_pandas_data, convert_to_r_data, import_model_class, init_model

#
# Tests for mvf.process.utils module.
#


@patch('builtins.open')
@patch('pickle.dump')
def test_process_timer(mock_dump, mock_open):
    '''
    Test the process timer.
    '''
    timer = ProcessTimer('test')
    time.sleep(1)
    timer.end()
    timer.save('test_path')

    assert timer.process_name == 'test'
    # Use a reasonable bound on execution time
    assert 1 < timer.process_time['test'] < 1.25, f"{timer.process_time['test']}"
    mock_open.assert_called_with('test_path', 'wb')
    mock_dump.assert_called()


@pytest.mark.parametrize(
    ('in_arg, exp'),
    [
        (
            'Python',
            {
                'call_count': 1,
                'return_type': MagicMock,
            }
        ),
        (
            'R',
            {
                'call_count': 2,
                'return_type': dict,
            }
        ),
    ]
)
@patch('builtins.__import__')
def test_import_model_class(mock_import, in_arg, exp):
    '''
    Test model import parameter logic.
    '''
    model_class = import_model_class(in_arg)

    assert mock_import.call_count == exp['call_count']
    assert isinstance(model_class, exp['return_type'])


# Load R model from test data
r = robjects.r
r['source']('test/test_resources/test_data/models.R')


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            [pandas, 'Python', 'DataFrame'],
            'equals'
        ),
        (
            [{'r': r, 'r6b': r6b}, 'R', 'r_pois_reg'],
            'fit'
        )
    ]
)
def test_init_model(in_args, exp):
    '''
    Test model initialisation. Check that the instantiated class has the expected method.
    '''
    model = init_model(*in_args)

    assert hasattr(model, exp)


@pytest.mark.parametrize(
    ('input_data', 'expected_output'),
    [
        (
            pandas.DataFrame([[0, 1]], columns=['a', 'b'], index=['1']),
            robjects.DataFrame({'a': 0, 'b': 1})
        )
    ]
)
def test_convert_to_r_data(input_data, expected_output):
    '''
    Tests conversion to R dataframe. Uses round trip in order to leverage
    pandas.DataFrame.equals().

    It is known that indices are not preserved. This is tested by the
    end to end k-fold test.
    '''
    output = convert_to_r_data(input_data)

    with (robjects.default_converter + pandas2ri.converter).context():
        assert robjects.conversion.get_conversion().rpy2py(output).equals(
            robjects.conversion.get_conversion().rpy2py(expected_output))


@pytest.mark.parametrize(
    ('input_data', 'expected_output'),
    [
        (
            robjects.DataFrame({'a': 0, 'b': 1}),
            pandas.DataFrame([[0, 1]], columns=['a', 'b'], index=['1'], dtype='int32')
        )
    ]
)
def test_convert_to_pandas_data(input_data, expected_output):
    '''
    Test conversion to pandas.

    It is known that indices are not preserved. This is tested by the
    end to end k-fold test.
    '''
    output = convert_to_pandas_data(input_data)

    assert output.equals(expected_output)
