import numpy
import pandas
from mvf.process import validation
import pytest
from unittest.mock import patch


@pytest.mark.parametrize(
    ('in_args', 'call_count'),
    [
        (
            (
                {
                    'split_data': {
                        'test_y_data': 'some_path'
                    }
                },
                'train_test'
            ),
            1
        ),
        (
            (
                {
                    'split_data': {
                        'fold_1_y_data': 'some_path1',
                        'fold_2_y_data': 'some_path2',
                        'fold_3_y_data': 'some_path3',
                    }
                },
                'k_fold',
                3
            ),
            3
        ),
    ]
)
@patch('feather.read_dataframe')
def test_load_validation_data(mock_reader, in_args, call_count):
    '''
    Test parameter logic and concatenation behaviour.
    '''
    mock_reader.return_value = pandas.DataFrame([0])

    validation_data = validation.load_validation_data(*in_args)

    assert mock_reader.call_count == call_count
    assert validation_data.shape[0] == call_count


@pytest.mark.parametrize(
    ('in_args', 'call_count'),
    [
        (
            (
                {
                    'm1_predict': {
                        'predictions': 'some_path'
                    }
                },
                ['m1']
            ),
            1
        ),
        (
            (
                {
                    'm1_predict': {
                        'predictions': 'some_path1'
                    },
                    'm2_predict': {
                        'predictions': 'some_path2'
                    }
                },
                ['m1', 'm2']
            ),
            2
        ),
    ]
)
@patch('feather.read_dataframe')
def test_load_predictions(mock_reader, in_args, call_count):
    '''
    Test parameter logic and output format.
    '''
    mock_reader.return_value = pandas.DataFrame()

    predictions = validation.load_predictions(*in_args)

    assert mock_reader.call_count == call_count
    assert isinstance(predictions, dict)
    assert sorted(list(predictions.keys())) == sorted(in_args[1])
    for pred in predictions.values():
        assert isinstance(pred, pandas.DataFrame)


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            (
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {'y1': numpy.arange(25)}
                    )
                },
                # validation_data
                pandas.DataFrame(
                    {'y1': numpy.arange(25)}
                )
            ),
            {
                'disp_count': 1,
                'df_index': ['m1']
            }
        ),
        (
            (
                # target_features
                ['y1', 'y2'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.arange(25),
                            'y2': numpy.arange(25)
                        }
                    )
                },
                # validation_data
                pandas.DataFrame(
                    {
                        'y1': numpy.arange(25),
                        'y2': numpy.arange(25)
                    }
                )
            ),
            {
                'disp_count': 2,
                'df_index': ['m1']
            }
        ),
        (
            (
                # target_features
                ['y1', 'y2'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.arange(25),
                            'y2': numpy.arange(25)
                        }
                    ),
                    'm2': pandas.DataFrame(
                        {
                            'y1': numpy.arange(25),
                            'y2': numpy.arange(25)
                        }
                    )
                },
                # validation_data
                pandas.DataFrame(
                    {
                        'y1': numpy.arange(25),
                        'y2': numpy.arange(25)
                    }
                )
            ),
            {
                'disp_count': 2,
                'df_index': ['m1', 'm2']
            }
        )
    ]
)
@patch('mvf.process.validation.display')
def test_error_metrics(mock_display, in_args, exp):
    '''
    Test output format. No need to test that errors are correctly calculated.
    '''
    validation.error_metrics(*in_args)

    assert mock_display.call_count == exp['disp_count']
    for c in mock_display.call_args_list:
        assert numpy.array_equal(c[0][0].index, exp['df_index'])


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            (
                # validation_data
                pandas.DataFrame(
                    {'y1': numpy.zeros(1)}
                ),
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 1,
                'coverage': [numpy.array([[0.8, 0]])]
            }
        ),
        (
            (
                # validation_data
                pandas.DataFrame(
                    {'y1': numpy.array([numpy.NAN, 0, 1])}
                ),
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.array([0, 0, 0]),
                            'y1_Q10': numpy.array([-0.5, -0.5, -0.5]),
                            'y1_Q90': numpy.array([0.5, 0.5, 0.5])
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 1,
                'coverage': [numpy.array([[0.8, 0.5]])]
            }
        ),
        (
            (
                # validation_data
                pandas.DataFrame(
                    {'y1': numpy.zeros(1)}
                ),
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4)
                        }
                    )
                },
                # quantile_intervals
                []
            ),
            {
                'disp_count': 0,
                'coverage': []
            }
        ),
        (
            (
                # validation_data
                pandas.DataFrame(
                    {'y1': numpy.zeros(1)}
                ),
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4)
                        }
                    ),
                    'm2': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 0),
                            'y1_Q10': numpy.full(1, -1),
                            'y1_Q90': numpy.full(1, 1)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 1,
                'coverage': [
                    numpy.array(
                        [
                            [0.8, 0],
                            [0.8, 1],
                        ]
                    )
                ]
            }
        ),
        (
            (
                # validation_data
                pandas.DataFrame(
                    {
                        'y1': numpy.zeros(1),
                        'y2': numpy.zeros(1)
                    }
                ),
                # target_features
                ['y1', 'y2'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4),
                            'y2': numpy.full(1, 0),
                            'y2_Q10': numpy.full(1, -1),
                            'y2_Q90': numpy.full(1, 1)
                        }
                    ),
                    'm2': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 0),
                            'y1_Q10': numpy.full(1, -1),
                            'y1_Q90': numpy.full(1, 1),
                            'y2': numpy.full(1, 0),
                            'y2_Q10': numpy.full(1, -1),
                            'y2_Q90': numpy.full(1, 1)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 2,
                'coverage': [
                    numpy.array(
                        [
                            [0.8, 0],
                            [0.8, 1],
                        ]
                    ),
                    numpy.array(
                        [
                            [0.8, 1],
                            [0.8, 1],
                        ]
                    )
                ]
            }
        )
    ]
)
@patch('mvf.process.validation.display')
def test_qi_coverage(mock_display, in_args, exp):
    '''
    Tests calculation of coverage metrics.
    '''
    validation.qi_coverage(*in_args)

    assert mock_display.call_count == exp['disp_count']

    for c, cov in zip(mock_display.call_args_list, exp['coverage']):
        assert numpy.array_equal(c[0][0].values, cov), f'{c[0][0]}'


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            (
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 1,
                'sharpness': [
                    numpy.array(
                        [
                            [2]
                        ]
                    )
                ]
            }
        ),
        (
            (
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4)
                        }
                    )
                },
                # quantile_intervals
                []
            ),
            {
                'disp_count': 0,
                'sharpness': []
            }
        ),
        (
            (
                # target_features
                ['y1'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4),
                            'y1_Q5': numpy.full(1, 1),
                            'y1_Q95': numpy.full(1, 5)
                        }
                    )
                },
                # quantile_intervals
                [
                    [0.1, 0.9],
                    [0.1, 0.95],
                    [0.05, 0.9],
                    [0.05, 0.95],
                ]
            ),
            {
                'disp_count': 1,
                'sharpness': [
                    numpy.array(
                        [
                            [2, 3, 3, 4]
                        ]
                    )
                ]
            }
        ),
        (
            (
                # target_features
                ['y1', 'y2'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4),
                            'y2': numpy.full(1, 3),
                            'y2_Q10': numpy.full(1, 0),
                            'y2_Q90': numpy.full(1, 5)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 2,
                'sharpness': [
                    numpy.array(
                        [
                            [2]
                        ]
                    ),
                    numpy.array(
                        [
                            [5]
                        ]
                    )
                ]
            }
        ),
        (
            (
                # target_features
                ['y1', 'y2'],
                # predictions
                {
                    'm1': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4),
                            'y2': numpy.full(1, 3),
                            'y2_Q10': numpy.full(1, 2.5),
                            'y2_Q90': numpy.full(1, 5)
                        }
                    ),
                    'm2': pandas.DataFrame(
                        {
                            'y1': numpy.full(1, 3),
                            'y1_Q10': numpy.full(1, 2),
                            'y1_Q90': numpy.full(1, 4),
                            'y2': numpy.full(1, 3),
                            'y2_Q10': numpy.full(1, 0),
                            'y2_Q90': numpy.full(1, 5)
                        }
                    )
                },
                # quantile_intervals
                [[0.1, 0.9]]
            ),
            {
                'disp_count': 2,
                'sharpness': [
                    numpy.array(
                        [
                            [2],
                            [2]
                        ]
                    ),
                    numpy.array(
                        [
                            [2.5],
                            [5]
                        ]
                    )
                ]
            }
        )
    ]
)
@patch('mvf.process.validation.display')
def test_qi_sharpness(mock_display, in_args, exp):
    '''
    Tests calculation of sharpness metrics.
    '''
    validation.qi_sharpness(*in_args)

    assert mock_display.call_count == exp['disp_count']

    for c, sharp in zip(mock_display.call_args_list, exp['sharpness']):
        assert numpy.array_equal(c[0][0].values, sharp), f'{c[0][0]}'


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            {
                'upstream': {
                    'py_bayes_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_bayes_reg_fit.metadata'
                    },
                    'py_lin_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_lin_reg_fit.metadata'
                    },
                    'r_pois_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/r_pois_reg_fit.metadata'
                    },
                    'py_bayes_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_bayes_reg_predict.metadata'
                    },
                    'py_lin_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_lin_reg_predict.metadata'
                    },
                    'r_pois_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/r_pois_reg_predict.metadata'
                    },
                },
                'models': [
                    'py_bayes_reg',
                    'py_lin_reg',
                    'r_pois_reg'
                ],
                'split_type': 'train_test'
            },
            numpy.array(
                [
                    [0.0532737, 0.04276227, 5.0],
                    [0.01935803, 0.02258734, 5.0],
                    [1.23794977, 0.44124894, 5.0]
                ]
            )
        ),
        (
            {
                'upstream': {
                    'py_bayes_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_bayes_reg_fit.metadata'
                    },
                    'py_lin_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_lin_reg_fit.metadata'
                    },
                    'r_pois_reg_fit': {
                        'process_metadata': 'test/test_resources/test_data/metadata/r_pois_reg_fit.metadata'
                    },
                    'py_bayes_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_bayes_reg_predict.metadata'
                    },
                    'py_lin_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/py_lin_reg_predict.metadata'
                    },
                    'r_pois_reg_predict': {
                        'process_metadata': 'test/test_resources/test_data/metadata/r_pois_reg_predict.metadata'
                    },
                },
                'models': [
                    'py_bayes_reg',
                    'py_lin_reg',
                    'r_pois_reg'
                ],
                'split_type': 'k_fold'
            },
            numpy.array(
                [
                    [0.0532737, 0.04276227, 50],
                    [0.01935803, 0.02258734, 50],
                    [1.23794977, 0.44124894, 50]
                ]
            )
        )
    ]
)
@patch('os.path.getsize')
@patch('mvf.process.validation.display')
def test_computational_performance(mock_display, mock_size, in_args, exp):
    '''
    Test aggregation of computational performance metadata.
    '''
    mock_size.return_value = 5
    validation.computational_performance(**in_args)

    assert (mock_display.call_args[0][0].values - exp).sum() < 1e-2
    assert list(mock_display.call_args[0][0].index) == in_args['models']
    assert list(mock_display.call_args[0][0].columns) == ['fit time', 'predict time', 'size']

