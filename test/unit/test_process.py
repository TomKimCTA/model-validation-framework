from unittest.mock import MagicMock, call, patch
import numpy

import pytest
from mvf.process.split_data import split_data, y_temporal_split
from mvf.process.predict_model import make_prediction, predict_model
from mvf.process.fit_model import fit, fit_model
import pandas
#
# Tests for DAG processes.
#


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            {
                'product': {
                    'train_X_data': '',
                    'train_y_data': '',
                    'test_X_data': '',
                    'test_y_data': '',
                },
                'split_type': 'train_test',
                'test_size': 0.3
            },
            {
                'writer_call_count': 4,
                'print_calls': [
                    'X_train shape: (5319, 4)',
                    'X_test shape: (2280, 4)',
                    'y_train shape: (5319, 2)',
                    'y_test shape: (2280, 2)',
                ],
                'random_call': False
            }
        ),
        (
            {
                'product': {
                    'train_X_data': '',
                    'train_y_data': '',
                    'test_X_data': '',
                    'test_y_data': '',
                },
                'split_type': 'train_test',
                'test_size': 0.3,
                'grouping_variable': 'group',
                'temporal_split': {
                    'temporal_variable': 'X1',
                    'test_size': 0.5
                }
            },
            {
                'writer_call_count': 4,
                'print_calls': [
                    'X_train shape: (7599, 4)',
                    'y_train shape: (7599, 2)',
                ],
                'random_call': False
            }
        ),
        (
            {
                'product': {
                    'train_X_data': '',
                    'train_y_data': '',
                    'test_X_data': '',
                    'test_y_data': '',
                },
                'split_type': 'train_test',
                'test_size': 0.3,
                'grouping_variable': 'group',
                'temporal_split': {
                    'temporal_variable': 'X1',
                    'test_size': [0.2, 0.9]
                }
            },
            {
                'writer_call_count': 4,
                'print_calls': [
                    'X_train shape: (7599, 4)',
                    'y_train shape: (7599, 2)',
                ],
                'random_call': True
            }
        ),
        (
            {
                'product': {
                    'fold_1_X_data': '',
                    'fold_1_y_data': '',
                    'fold_2_X_data': '',
                    'fold_2_y_data': '',
                    'fold_3_X_data': '',
                    'fold_3_y_data': '',
                },
                'split_type': 'k_fold',
                'n_folds': 3
            },
            {
                'writer_call_count': 6,
                'print_calls': [
                    'Fold 1 X shape: (2533, 4)',
                    'Fold 1 y shape: (2533, 2)',
                    'Fold 2 X shape: (2533, 4)',
                    'Fold 2 y shape: (2533, 2)',
                    'Fold 3 X shape: (2533, 4)',
                    'Fold 3 y shape: (2533, 2)',
                ],
                'random_call': False
            }
        ),
    ]
)
@patch('numpy.random.uniform')
@patch('builtins.print')
@patch('feather.write_dataframe')
def test_split_data(mock_writer, mock_print, mock_random, in_args, exp):
    '''
    Tests parameter logic for split_data process. Functionality is tested
    by the mvf.integration module and end-to-end tests.
    '''
    mock_random.return_value = 0.5
    split_data(
        {
            'preprocess_data': {
                'X_data': 'test/test_resources/test_data/preprocess_X_data.feather',
                'y_data': 'test/test_resources/test_data/preprocess_y_data.feather'
            }
        },
        **in_args
    )

    assert mock_writer.call_count == exp['writer_call_count']
    assert mock_random.called == exp['random_call']
    print_calls = [c[0][0] for c in mock_print.call_args_list]
    assert set(exp['print_calls']) <= set(print_calls)


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            (
                # X_test
                pandas.DataFrame({
                    'group': numpy.concatenate([
                        numpy.full(10, 1),
                        numpy.full(5, 2)
                    ]),
                    'X1': numpy.concatenate([
                        numpy.arange(10),
                        numpy.arange(5)
                    ]),
                }),
                # y_test
                pandas.DataFrame({
                    'y1': numpy.array(
                        [
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                            9,
                            10,
                            1,
                            2,
                            numpy.nan,
                            numpy.nan,
                            numpy.nan,
                        ]
                    )
                }),
                # temp_var
                'X1',
                # grouping_variable
                'group',
                # test_size
                0.5
            ),
            {
                'train': pandas.DataFrame([
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    9,
                    numpy.nan,
                    1,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                ]),
                'test': pandas.DataFrame([
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                    10,
                    numpy.nan,
                    2,
                    numpy.nan,
                    numpy.nan,
                    numpy.nan,
                ]),
            }
        ),
    ]
)
def test_y_temporal_split(in_args, exp):
    '''
    Tests the temporal split of hierarchical time series.
    '''
    y_test, y_train_sup = y_temporal_split(*in_args)

    assert numpy.array_equal(y_test.notna(), exp['test'].notna())
    assert numpy.array_equal(y_train_sup.notna(), exp['train'].notna())


@pytest.mark.parametrize(
    ('params', 'expected'),
    [
        (
            {
                'upstream': {
                    'split_data': {
                        'test_X_data': ''
                    },
                    'model_fit': {
                        'model': ''
                    }
                },
                'product': {
                    'predictions': '',
                    'process_metadata': '',
                },
                'lang': 'Python',
                'model_name': 'model',
                'split_type': 'train_test',
            },
            {
                'lang': 'Python',
                'call_count': 1
            }
        ),
        (
            {
                'upstream': {
                    'split_data': {
                        'fold_1_X_data': '',
                        'fold_2_X_data': '',
                        'fold_3_X_data': '',
                    },
                    'model_fit': {
                        'model_1': '',
                        'model_2': '',
                        'model_3': '',
                    }
                },
                'product': {
                    'predictions': '',
                    'process_metadata': '',
                },
                'lang': 'R',
                'model_name': 'model',
                'split_type': 'k_fold',
                'n_folds': 3,
            },
            {
                'lang': 'R',
                'call_count': 3
            }
        ),
    ]
)
@patch('feather.write_dataframe')
@patch('mvf.process.predict_model.ProcessTimer')
@patch('mvf.process.predict_model.make_prediction')
@patch('mvf.process.predict_model.import_model_class')
def test_predict_model(mock_importer, mock_predicter, mock_timer, mock_writer, params, expected):
    '''
    Test parameter logic for predict_model process.
    '''
    mock_predicter.return_value = pandas.DataFrame()
    predict_model(**params)

    mock_timer.assert_called_once()
    mock_importer.assert_called_with(expected['lang'])
    assert mock_predicter.call_count == expected['call_count']
    mock_writer.assert_called_once()


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            [
                'data_path',
                'model_path',
                'Python',
                'model_module',
                'model_name',
                None
            ],
            {
                'r_converter_count': 0,
                'pandas_converter_count': 0,
                'method_calls': [
                    call.load('model_path'),
                    call.predict('test_data', None)
                ]
            }
        ),
        (
            [
                'data_path',
                'model_path',
                'R',
                'model_module',
                'model_name',
                [[0.05, 0.95]]
            ],
            {
                'r_converter_count': 1,
                'pandas_converter_count': 1,
                'method_calls': [
                    call.load('model_path'),
                    call.predict('test_data', [[0.05, 0.95]])
                ]
            }
        ),
        (
            [
                'data_path',
                'model_path',
                'R',
                {'r': MagicMock(return_value='r_null')},
                'model_name',
                None
            ],
            {
                'r_converter_count': 1,
                'pandas_converter_count': 1,
                'method_calls': [
                    call.load('model_path'),
                    call.predict('test_data', 'r_null')
                ]
            }
        )
    ]
)
@patch('mvf.process.predict_model.convert_to_pandas_data')
@patch('mvf.process.predict_model.convert_to_r_data')
@patch('mvf.process.predict_model.init_model')
@patch('feather.read_dataframe')
def test_make_prediction(mock_reader, mock_init, mock_r_converter, mock_pandas_converter, in_args, exp):
    '''
    Test the make_prediction method.
    '''
    mock_reader.return_value = 'test_data'
    mock_r_converter.return_value = 'test_data'
    preds = make_prediction(*in_args)

    mock_reader.assert_called_with(in_args[0])
    mock_init.assert_called_with(in_args[3], in_args[2], in_args[4])
    assert mock_r_converter.call_count == exp['r_converter_count']
    assert mock_pandas_converter.call_count == exp['pandas_converter_count']
    assert mock_init().method_calls == exp['method_calls']


@pytest.mark.parametrize(
    ('params', 'expected'),
    [
        (
            {
                'upstream': {
                    'split_data': {
                        'train_X_data': '',
                        'train_y_data': '',
                    },
                },
                'product': {
                    'model': '',
                    'process_metadata': '',
                },
                'lang': 'Python',
                'model_name': 'model',
                'split_type': 'train_test',
            },
            {
                'lang': 'Python',
                'fit_call_count': 1,
                'reader_call_count': 2,
            }
        ),
        (
            {
                'upstream': {
                    'split_data': {
                        'fold_1_X_data': '',
                        'fold_2_X_data': '',
                        'fold_3_X_data': '',
                        'fold_1_y_data': '',
                        'fold_2_y_data': '',
                        'fold_3_y_data': '',
                    },
                },
                'product': {
                    'model_1': '',
                    'model_2': '',
                    'model_3': '',
                    'process_metadata': '',
                },
                'lang': 'R',
                'model_name': 'model',
                'split_type': 'k_fold',
                'n_folds': 3,
            },
            {
                'lang': 'R',
                'fit_call_count': 3,
                'reader_call_count': 6,
            }
        ),
    ]
)
@patch('feather.read_dataframe')
@patch('mvf.process.fit_model.ProcessTimer')
@patch('mvf.process.fit_model.fit')
@patch('mvf.process.fit_model.import_model_class')
def test_fit_model(mock_importer, mock_fitter, mock_timer, mock_reader, params, expected):
    '''
    Test parameter logic for fit_model process.
    '''
    mock_reader.return_value = pandas.DataFrame()
    mock_fitter.return_value = pandas.DataFrame()

    fit_model(**params)

    mock_timer.assert_called_once()
    mock_importer.assert_called_with(expected['lang'])
    assert mock_fitter.call_count == expected['fit_call_count']
    assert mock_reader.call_count == expected['reader_call_count']


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            [
                'X_train',
                'y_train',
                'model_path',
                'model_module',
                'R',
                'model_name'
            ],
            {
                'converter_calls': 2,
                'init_call_args': (
                    'model_module',
                    'R',
                    'model_name'
                ),
                'method_calls': [
                    call.fit('r_data', 'r_data'),
                    call.save('model_path')
                ]
            }
        ),
        (
            [
                'X_train',
                'y_train',
                'model_path',
                'model_module',
                'Python',
                'model_name'
            ],
            {
                'converter_calls': 0,
                'init_call_args': (
                    'model_module',
                    'Python',
                    'model_name'
                ),
                'method_calls': [
                    call.fit('X_train', 'y_train'),
                    call.save('model_path')
                ]
            }
        )
    ]
)
@patch('mvf.process.fit_model.init_model')
@patch('mvf.process.fit_model.convert_to_r_data')
def test_fit(mock_converter, mock_init, in_args, exp):
    '''
    Test the fit method.
    '''
    mock_converter.return_value = 'r_data'

    fit(*in_args)

    assert mock_converter.call_count == exp['converter_calls']
    mock_init.assert_called_with(*exp['init_call_args'])
    assert mock_init(
    ).method_calls == exp['method_calls'], f'{mock_init().method_calls}'
