import pytest
import yaml
from mvf.integration.mvf_config import check_config

#
# Tests for cli.integration.mvf_config module
#


@pytest.mark.parametrize(
    ('config_path'),
    [
        ('test/test_resources/test_py_preprocess_train_test/mvf_conf.yaml'),
        ('test/test_resources/test_r_preprocess_kfold/mvf_conf.yaml'),
        ('test/test_resources/test_kfold_end_to_end/mvf_conf.yaml'),
    ]
)
def test_check_config_valid(config_path):
    '''
    Tests that configs for example projects are valid.
    '''
    with open(config_path, 'r') as f:
        config = yaml.safe_load(f)
    check_config(config)


@pytest.mark.parametrize(
    ('config', 'exp_err'),
    # each example config is invalid in a different way
    [
        (
            # missing key
            {
                'data': {
                    # here
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python'
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Missing key: \'source\''
        ),
        (
            # invalid source file extension
            {
                'data': {
                    'source': 'preprocess.py',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python'
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Invalid `source` file extension. Must be ipynb.'
        ),
        (
            # unspecified key
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    # here
                    'something_extra': 'not_wanted',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python'
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Wrong key \'something_extra\''
        ),
        (
            # concurrent parameters
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    # here
                    'n_folds': 10,
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Only one of `test_size` and `n_folds` may be specified.'
        ),
        (
            # invalid 'or' option
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    # here
                    'split': 'unknown_option',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Key \'split\' error'
        ),
        (
            # invalid test_size
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    # here
                    'test_size': 1.1,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Key \'test_size\' error'
        ),
        (
            # invalid value type
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        # here
                        'name': 27,
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            'Key \'name\' error'
        ),
        (
            # mismatching keys
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    # here
                    'split': 'train_test',
                    'n_folds': 10,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            '`n_folds` is not a valid parameter for a \'train_test\' split.'
        ),
        (
            # mismatching keys
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    # here
                    'split': 'k_fold',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True
                    }
                ]
            },
            '`test_size` is not a valid parameter for a \'k_fold\' split.'
        ),
        (
            # incorrect quantile specification
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'output': {
                    'quantile_intervals':
                    [
                        # here
                        [0.1],
                    ],
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False,
                        'return_quantiles': True,
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True,
                        'return_quantiles': False,
                    }
                ]
            },
            'Two quantiles must be provided for each interval.'
        ),
        (
            # model quantile specification missing
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y']
                },
                'output': {
                    'quantile_intervals':
                    [
                        [0.1, 0.9],
                    ],
                },
                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python',
                        'validation_step': False,
                        # here
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R',
                        'validation_step': True,
                        'return_quantiles': False,
                    }
                ]
            },
            'If `quantile_intervals` are passed in `output`, the `return_quantiles` parameter must be passed for each model.'
        ),
        (
            # temporal split params provided but no grouping_variable
            {
                'data': {
                    'source': 'preprocess_data.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                    'input_features': ['X'],
                    'target_features': ['y'],
                    # here
                    'temporal_split': {
                        'temporal_variable': 'some_var',
                        'test_size': 0.5
                    }
                },

                'models': [
                    {
                        'name': 'py_lin_reg',
                        'lang': 'Python'
                    },
                    {
                        'name': 'r_pois_reg',
                        'lang': 'R'
                    }
                ]
            },
            'If the `temporal_split` parameters are provided, a grouping variable must also be provided.'
        ),
    ]
)
def test_check_config_invalid(config, exp_err):
    '''
    Tests that errors are raised when invalid configs are passed.
    '''
    with pytest.raises(Exception, match=exp_err):
        check_config(config)
