from mvf.cli import cli
from mvf.cli.utils import load_config, download_example
from click.testing import CliRunner
import os
import pytest
from unittest.mock import MagicMock, patch, mock_open, call

#
# Tests for the mvf.cli module.
#


@pytest.mark.parametrize(
    ('in_args', 'exp'),
    [
        (
            ['init'],
            {
                'pth': os.getcwd(),
                'exit_code': 0,
                'mkdir_called': False
            }
        ),
        (
            ['init', '-d', os.getcwd()],
            {
                'pth': os.getcwd(),
                'exit_code': 0,
                'mkdir_called': False
            }
        ),
        (
            ['init', '-d', 'test'],
            {
                'pth': os.path.join(os.getcwd(), 'test'),
                'exit_code': 0,
                'mkdir_called': False
            }
        ),
        (
            ['init', '-d', 'not_a_directory'],
            {
                'pth': os.path.join(os.getcwd(), 'not_a_directory'),
                'exit_code': 0,
                'mkdir_called': True
            }
        ),
        (
            ['init', '-d', 'version'],
            {
                'pth': os.path.join(os.getcwd(), 'test'),
                'exit_code': 1,
                'mkdir_called': False
            }
        )
    ]
)
@patch('mvf.cli.cli.ConfigBuilder')
@patch('click.confirm')
@patch('os.mkdir')
def test_cli_init(mock_mkdir, mock_confirm, mock_builder, in_args, exp):
    '''
    Tests the path configuration/checking for different CLI options for init command.
    '''
    mock_confirm.return_value = True

    runner = CliRunner()
    result = runner.invoke(cli.mvf, in_args)

    assert result.exit_code == exp['exit_code'], f'{result.exc_info}'
    if exp['exit_code'] == 0:
        assert exp['pth'] in mock_builder.call_args.args, f'{mock_builder.call_args}'
        assert exp['mkdir_called'] == mock_mkdir.called


@pytest.mark.parametrize(
    ('cfg', 'exp'),
    [
        (
            {
                'data': {
                    'source': 'path_to_your_source_code.ipynb',
                    'lang': 'Python',
                    'split': 'train_test',
                    'test_size': 0.3,
                },
                'models': [
                    {
                        'name': 'your_model_name',
                        'lang': 'Python'
                    },
                ],
            },
            [
                call.gen_preprocess(
                    'path_to_your_source_code.ipynb', 'Python'),
                call.gen_models_py(['your_model_name']),
                call.gen_models_r([])
            ]
        ),
        (
            {
                'data': {
                    'source': 'other_path_to_your_source_code.ipynb',
                    'lang': 'Python',
                    'split': 'k_fold',
                    'n_folds': 10,
                },
                'models': [
                    {
                        'name': 'r_mod',
                        'lang': 'R',
                        'validation_step': True,
                    },
                    {
                        'name': 'py_mod',
                        'lang': 'Python'
                    },
                ],
            },
            [
                call.gen_preprocess(
                    'other_path_to_your_source_code.ipynb', 'Python'),
                call.gen_models_py(['py_mod']),
                call.gen_models_r(['r_mod'])
            ]
        ),
        (
            {
                'data': {
                    'source': 'other_path.ipynb',
                    'lang': 'R',
                    'split': 'k_fold',
                    'n_folds': 10,
                },
                'models': [
                    {
                        'name': 'r_mod1',
                        'lang': 'R',
                        'validation_step': True,
                    },
                    {
                        'name': 'r_mod2',
                        'lang': 'R'
                    },
                ],
            },
            [
                call.gen_preprocess('other_path.ipynb', 'R'),
                call.gen_models_py([]),
                call.gen_models_r(['r_mod1', 'r_mod2'])
            ]
        )
    ]
)
@patch('mvf.cli.cli.FileGenerator')
@patch('click.confirm')
@patch('mvf.cli.utils.load_config')
def test_cli_scaffold(mock_config_loader, mock_confirm, mock_file_gen, cfg, exp):
    '''
    Tests the CLI scaffold command. Check correct methods are called given config.
    '''
    mock_generator = MagicMock()
    mock_confirm.return_value = True
    mock_config_loader.return_value = cfg
    mock_file_gen.return_value = mock_generator

    runner = CliRunner()
    result = runner.invoke(cli.mvf, ['scaffold'])

    assert result.exit_code == 0, f'{result.exc_info}'
    mock_generator.method_calls == exp


@pytest.mark.parametrize(
    ('in_args', 'exp_arg'),
    [
        (
            ['run'],
            {'force': False},
        ),
        (
            ['run', '-f'],
            {'force': True},
        ),
    ]
)
@patch('mvf.cli.cli.DagBuilder')
@patch('mvf.cli.utils.load_config')
def test_cli_run_dag(mock_config_loader, mock_dag_builder, in_args, exp_arg):
    '''
    Tests logic for whole dag CLI options for run command.
    '''
    runner = CliRunner()
    result = runner.invoke(cli.mvf, in_args)

    assert mock_config_loader.called
    assert mock_dag_builder.called
    assert mock_dag_builder().build.called
    assert mock_dag_builder().dag.build.call_args.kwargs == exp_arg
    assert result.exit_code == 0, f'{result.exception}'


@pytest.mark.parametrize(
    ('in_args', 'mock_return'),
    [
        (
            ['run', '-p', 'process_name'],
            MagicMock()
        ),
        (
            ['run', '-p', 'not_a_process_name'],
            None
        ),
    ]
)
@patch('mvf.cli.cli.DagBuilder')
@patch('mvf.cli.utils.load_config')
def test_cli_run_process(mock_config_loader, mock_dag_builder, in_args, mock_return):
    '''
    Tests logic for -p options for CLI run command.
    '''
    mock_dag_builder().dag.get.return_value = mock_return
    runner = CliRunner()
    result = runner.invoke(cli.mvf, in_args)

    assert mock_config_loader.called
    assert mock_dag_builder.called

    assert result.exit_code == 0, f'{result.exception}'
    if mock_return is not None:
        assert mock_return.build.called, f'{mock_return}'
        assert mock_return.build.call_args.kwargs == {'force': True}
    else:
        assert mock_dag_builder().dag.keys.called


@pytest.mark.parametrize(
    ('in_args', 'exp_arg'),
    [
        (
            ['plot'],
            {'output_dir': 'output'},
        ),
        (
            ['plot', '-o', 'test'],
            {'output_dir': 'test'},
        ),
    ]
)
@patch('mvf.cli.cli.DagBuilder')
@patch('mvf.cli.utils.load_config')
def test_cli_plot(mock_config_loader, mock_dag_builder, in_args, exp_arg):
    '''
    Tests configuration of output path for CLI plot command.
    '''
    runner = CliRunner()
    result = runner.invoke(cli.mvf, in_args)

    assert mock_config_loader.called
    assert result.exit_code == 0, f'{result.exc_info}'
    assert exp_arg == mock_dag_builder.call_args.kwargs, f'{mock_dag_builder.call_args.kwargs}'


@pytest.mark.parametrize(
    ('in_args', 'exp_args'),
    [
        (
            ['examples', '-l'],
            {
                'echo_call': call('The available examples are\n'),
                'downloader_call': False
            },
        ),
        (
            ['examples'],
            {
                'echo_call': call('Usage: mvf examples [OPTIONS]\n\n  Try out an example project.\n\nOptions:\n  -l, --list         List the available projects.\n  -n, --name TEXT    Name of project to download.\n  -o, --output TEXT  Target directory.\n  --help             Show this message and exit.'),
                'downloader_call': False
            },
        ),
        (
            ['examples', '-n', 'garbage'],
            {
                'echo_call': call('There is no project named garbage. Check the available projects with\n\n    mvf examples -l\n'),
                'downloader_call': False
            },
        ),
        (
            ['examples', '-n', 'feature_scaling'],
            {
                'echo_call': call('Downloading feature_scaling project...'),
                'downloader_call': ('feature_scaling', 'feature_scaling')
            },
        ),
        (
            ['examples', '-n', 'feature_scaling', '-o', 'custom_dir'],
            {
                'echo_call': call('Downloading feature_scaling project...'),
                'downloader_call': ('feature_scaling', 'custom_dir')
            },
        ),
    ]
)
@patch('mvf.cli.utils.download_example')
@patch('click.echo')
def test_cli_examples(mock_echo, mock_downloader, in_args, exp_args):
    '''
    Tests logic for flags and options for CLI examples command.
    '''
    runner = CliRunner()
    result = runner.invoke(cli.mvf, in_args)

    assert result.exit_code == 0, f'{result.exc_info}'
    mock_echo.assert_has_calls([exp_args['echo_call']])
    if exp_args['downloader_call']:
        mock_downloader.assert_called_with(*exp_args['downloader_call'])


def test_load_config_error():
    '''
    Test error handling for config loading util.
    '''
    with pytest.raises(Exception):
        load_config('')


@patch('mvf.cli.utils.mvf_config.check_config')
@patch('builtins.open', new_callable=mock_open, read_data='{}')
def test_load_config(mock_open, mock_config_checker):
    '''
    Test load config.
    '''
    res = load_config('')

    assert mock_open.called
    assert mock_config_checker.call_args.args[0] == {}


@pytest.mark.parametrize(
    ('in_arg', 'exp_arg'),
    [
        (
            ('example_name', 'example_name'),
            [
                call([
                    'git',
                    'clone',
                    '-n',
                    '--depth',
                    '1',
                    '-b',
                    'main',
                    '--filter=blob:none',
                    'https://gitlab.com/TomKimCTA/model-validation-framework',
                    'example_name'
                ]),
                call([
                    'git',
                    'sparse-checkout',
                    'set',
                    'test/test_resources/test_example_name'
                ]),
                call([
                    'git',
                    'checkout'
                ])
            ]
        ),
        (
            ('a_different_name', 'different_target'),
            [
                call([
                    'git',
                    'clone',
                    '-n',
                    '--depth',
                    '1',
                    '-b',
                    'main',
                    '--filter=blob:none',
                    'https://gitlab.com/TomKimCTA/model-validation-framework',
                    'different_target'
                ]),
                call([
                    'git',
                    'sparse-checkout',
                    'set',
                    'test/test_resources/test_a_different_name'
                ]),
                call([
                    'git',
                    'checkout'
                ])
            ]
        ),
    ]
)
@patch('os.remove')
@patch('distutils.dir_util.remove_tree')
@patch('distutils.dir_util.copy_tree')
@patch('os.chdir')
@patch('subprocess.run')
def test_download_example(mock_subprocess, mock_chdir, mock_copy_tree, mock_remove_tree, mock_os_rm, in_arg, exp_arg):
    '''
    Test download_example subprocess arguments.
    '''
    download_example(*in_arg)

    mock_subprocess.assert_has_calls(exp_arg)
    mock_chdir.assert_called_once()
    mock_copy_tree.assert_called_once()
    assert mock_remove_tree.call_count == 2


@pytest.mark.parametrize(
    ('in_args', 'exp_err'),
    [
        (
            ('feature_scaling', os.getcwd()),
            'model-validation-framework is already a directory.'
        ),
        (
            ('feature_scaling', 'test'),
            'test is already a directory.'
        )
    ]
)
def test_download_example_error(in_args, exp_err):
    '''
    Test download_example error handling.
    '''
    with pytest.raises(Exception, match=exp_err) as e:
        download_example(*in_args)
