import numpy
from mvf.cli.cli import mvf
from click.testing import CliRunner
import os
import pytest
import sys
import feather
import pandas
import sklearn.metrics as error_metrics


@pytest.mark.parametrize(
    ('test_project_path'),
    [
        ('test/test_resources/test_kfold_end_to_end'),
        ('test/test_resources/test_temporal_split'),
        ('test/test_resources/test_grouped_train_test'),
        ('test/test_resources/test_grouped_kfold'),
        ('test/test_resources/test_py_preprocess_train_test'),
        ('test/test_resources/test_r_preprocess_kfold'),
        ('test/test_resources/test_feature_scaling'),
    ]
)
def test_end_to_end(test_project_path):
    '''
    Run test projects end-to-end. Call project specific methods to check outputs against expectation.
    '''
    testing_dir = os.getcwd()
    os.chdir(test_project_path)
    runner = CliRunner()

    try:
        # run project
        result = runner.invoke(mvf, ['run', '-f'])
        # smoke test
        assert result.exit_code == 0

        # call project specific test method
        if test_project_path == 'test/test_resources/test_kfold_end_to_end':
            kfold()
        if test_project_path == 'test/test_resources/test_grouped_train_test':
            grouped_train_test()
        if test_project_path == 'test/test_resources/test_grouped_kfold':
            grouped_kfold()
        if test_project_path == 'test/test_resources/test_temporal_split':
            temporal_split()
    finally:
        sys.modules.pop('models')
        sys.path.pop()
        os.chdir(testing_dir)


def kfold():
    '''
    Checks the error is 0 as expected.
    '''
    ground_truth = []
    for i in range(1, 11):
        ground_truth.append(
            feather.read_dataframe(f'output/data/fold_{i}_y_data.feather')
        )
    ground_truth = pandas.concat(ground_truth).reset_index(drop=True)

    predictions = {}
    for model_name in ['test_kfold_py', 'test_kfold_r']:
        predictions[model_name] = feather.read_dataframe(
            f'output/data/{model_name}_predict.feather'
        ).reset_index(drop=True)

    for pred in predictions.values():
        mean_preds = pred['y']
        assert error_metrics.mean_absolute_error(mean_preds, ground_truth) == 0


def grouped_train_test():
    '''
    Checks no grouping variable exists in both datasets.
    '''
    X_train = feather.read_dataframe('output/data/train_X_data.feather')
    X_test = feather.read_dataframe('output/data/test_X_data.feather')
    train_group = set(X_train.group)
    test_group = set(X_test.group)

    assert train_group.intersection(test_group) == set()


def grouped_kfold():
    '''
    Checks no grouping variable exists in multiple datasets.
    '''
    group_intersection = set()
    for i in range(1, 4):
        fold = feather.read_dataframe(f'output/data/fold_{i}_X_data.feather')
        group_set = set(fold.group)
        group_intersection = group_intersection.intersection(group_set)

    assert group_intersection == set()


def temporal_split():
    '''
    Checks time series have been split correctly.
    '''
    group_var = 'group'
    temp_var = 'X1'
    X_test = feather.read_dataframe('output/data/test_X_data.feather')
    X_train = feather.read_dataframe('output/data/train_X_data.feather')
    y_test = feather.read_dataframe('output/data/test_y_data.feather')
    y_train = feather.read_dataframe('output/data/train_y_data.feather')
    target_features = list(y_test.columns)
    
    test_data = pandas.concat([X_test[[temp_var, group_var]], y_test], axis=1)
    train_data = pandas.concat([X_train[[temp_var, group_var]], y_train], axis=1)
    merged = test_data.merge(
        train_data, 
        how='left', 
        on=[temp_var, group_var], 
        suffixes=['_test', '_train']
    )
    not_null = merged.set_index([group_var, temp_var]).notna().reset_index()

    agg = not_null.groupby(
        group_var, 
        dropna=False
    ).agg(
        ['min', 'max', 'sum']
    )
    agg['series_length'] = agg[(temp_var, 'max')] - agg[(temp_var, 'min')] + 1
    # Check the proportion of non-null values for the test time series is close to the specified proportion
    for target in target_features:
        assert (numpy.abs((agg[(f'{target}_test', 'sum')] / agg['series_length']) - 0.4) <= 1e-1).all()