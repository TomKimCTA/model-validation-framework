import sys
import pytest
from mvf.cli import cli
from click.testing import CliRunner
import os
import shutil


@pytest.mark.parametrize(
    ('in_args'),
    [
        (
            ['examples', '-n', 'feature_scaling', '-o', 'examples']
        ),
        (
            ['examples', '-n', 'py_preprocess_train_test', '-o', 'examples']
        ),
        (
            ['examples', '-n', 'r_preprocess_kfold', '-o', 'examples']
        )
    ]
)
def test_cli_examples(in_args):
    '''
    Functional tests for CLI examples command.

        1. Download example from GitLab.
        3. Execute `mvf run`.
    '''
    try:
        runner = CliRunner()
        result = runner.invoke(cli.mvf, in_args)
        assert result.exit_code == 0, f'{result.exc_info}'

        result = runner.invoke(cli.mvf, ['run'])
        assert result.exit_code == 0, f'{result.exc_info}'

    except Exception as err:
        exception = err
    else:
        exception = False
    finally:
        if 'models' in sys.modules:
            del sys.modules['models']
        if 'examples' in os.path.basename(os.getcwd()):
            os.chdir('..')
            shutil.rmtree('examples')
        if exception:
            raise exception
        
