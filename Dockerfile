FROM python:3.9

ENV DEBIAN_FRONTEND=noninteractive

# install Python and R
RUN apt-get update && apt-get install -y --no-install-recommends build-essential r-base libblas-dev liblapack-dev

# install project dependencies
RUN Rscript -e 'install.packages("R6")'
RUN Rscript -e 'install.packages("arrow")'
RUN Rscript -e 'install.packages("IRkernel")'
RUN Rscript -e 'install.packages("brms")'
ADD setup.py ./setup.py
ADD version ./version
ADD README.md ./README.md
RUN python3 -m pip install .[dev] > /dev/null
RUN Rscript -e 'IRkernel::installspec()'
