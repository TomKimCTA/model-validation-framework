## Background

ML software frameworks exist allowing users to define pipelines, train models, track metrics.

* MLFlow
* Ploomber
* AirFlow
* Amazon Sagemaker
* Neptune AI
* Valohai

These tools are flexible enough to accommodate theneeds of this project. (R/Python interoperable, UIs for managing different ML experiments, large community). In fact, MVF is built on top of Ploomber. On the other hand, their flexibility also creates a high barrier to entry. Interacting with researchers through these frameworks would demand that they learn complex APIs.

reference to MLFlow recipes


### CD4ML

* Development of candidate models
* Model selection
* When a model is in production, need to refit each time there is a change in
    * model architecture
    * code implementation
    * data
    Need mechanisms to ensure model performance does not degrade with these changes (ratcheting, threshold testing)
* Manage test data to ensure bias is not introduced over time.
* Monitor performance of production models.

### Motivation wrt DMD group

* Need a way to rigorously compare different approaches
    * useful for publication
* In a long running research group, there is a need for work to be picked up by new members of the group. Adoption of a common software framework, for validation purposes or otherwise, eases this transition. In particular, encouraging researches to adopt OOP practices reduces code repetition and allows for research to become modular.