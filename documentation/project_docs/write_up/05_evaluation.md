## Evaluation

### User story

* `mvf examples` command makes it very easy for a user to quickly interact with the tool.
* Documentation site provides support for users. Includes
    * Getting started tutorial
    * User guides for specific use cases
    * Full API documentation.

### Limitations & Further development

* Regression specific. Would need some extra development for classification problems.
* a single workflow is supported.
* The theoretical validation framework is under-developed.
* Error handling. Not currently user friendly. Checks on user provided configuration/code is not exhaustive.
* Generation of project files is not incremental.
* MLFlow integration for tracking.