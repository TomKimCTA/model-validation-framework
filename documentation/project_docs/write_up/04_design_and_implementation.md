# Design & Implementation

## Validation framework

Balance between generic regression (for the tool to be reusable by Certus - the business stakeholder) and bespoke validation for hierarchical time series regression models (to meet the needs of DMD modelling research group).

### Error metrics

Generic regression performance metrics

* MSE
* RMSE
* MAE
* MAPE
* R-Squared (coefficient of determination)

### Coverage

Metric for Bayesian prediction intervals. ~95% of ground truth falls within 95% prediction interval. (not applicable for frequentist CIs)[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6630113/].

### Sharpness

Average width of prediction intervals.

### Computational performance

Computational performance metrics

- fit time
- predict time
- model size (may vary - e.g. may choose to take more samples from a sampler)

### Plots

Specific to hierarchical time series regression problems. Plots predictions of various models against the true values used as inputs and the true values held out for validation.

## Software implementation

The validation framework was implemented as a Python package called MVF. Supports a standardised supervised machine learning workflow.

![image](/img/mvf_workflow.png)

A project is defined by a directory containing

* `mvf_conf.yaml` - the mvf configuration file
* source code to extract, preprocess and source the data - can be a Python/R notebook or R Markdown file.
* `models.py` - source code containing class definitions for any Python models defined in `mvf_conf.yaml`. May be omitted if no Python models are defined.
* `models.R` - source code containing class definitions for any R models defined in `mvf_conf.yaml`. May be omitted if no R models are defined.

Each model defined in `mvf_conf.yaml` must have a matching class definition in one of `models.py` or `models.R`. The model classes must have

* *fit()*
* *predict()*
* *save()*
* *load()*

methods.

There are `mvf init` and `mvf scaffold` commands which generate template project files for the user.

The user can then execute `mvf run` CLI command. This command will 

* read and validate the config in `mvf_conf.yaml`.
* build and execute the MVF workflow.

The MVF workflow draws source code from the project files and the `mvf` package. As the workflow is executed, integration tests are run between each process.

### Package structure

`mvf` is subdivided into 5 modules.

* `mvf.cli` contains defines the CLI commands.
* `mvf.dag` builds the MVF workflow object from user configuration.
* `mvf.process` contains source code for the standardised processes in the workflow.
* `mvf.integration` contains any integration tests that are executed during `mvf run`.
* `mvf.template` contains code and templates to generate project files.

### Tools

* The CLI was built using [Click](https://click.palletsprojects.com/en/8.1.x/).
* A schema for the project configuration was defined using [schema](https://github.com/keleshev/schema) and used to validate user defined configuration.
* [Ploomber](https://docs.ploomber.io/en/latest/) was used to build workflows. Of course, a user could use Ploomber to define their own workflows and create their own custom frameworks. Some, undoubtedly, already do.
* [Feather](https://arrow.apache.org/docs/python/feather.html) was used as a data format for its read/write speed and Python/R interoperability.
* Interoperability with R was supported using the [rpy2](https://rpy2.github.io/) and [rpy2-r6](https://rpy2.github.io/rpy2-r6/version/master/html/index.html) libraries. The latter specifically supports the interface between Python classes and R6 classes.
* [Pandas](https://pandas.pydata.org/docs/index.html) for data manipulation.
* [scikit-learn](https://scikit-learn.org/stable/index.html) for data splitting and error metrics.
* [Matplotlib](https://matplotlib.org/) for plotting.

### Python/R interoperability

Main technical challenge of the project. Alternative is a script based approach.

MVF is written in Python. Infrastructure, tests etc.

Users are able to compare the performance of R and Python models. MVF uses [Feather](https://github.com/wesm/feather) for all data storage. Feather allows for the storage of data frames in a format usable by both R and Python. This constrains MVF to use pandas.DataFrames in Python and data.frames in R. MVF is agnostic to the model object type.

Users must define a class for each of the models they wish to include. R models are defined in `models.R`, Python models defined in `models.py`. If the project only uses Python, `models.R` will be absent and vice versa.

In the following example, a user has created a class in `models.py` called `py_lin_reg` that implements a linear regression model. The user has also created a class in `models.R` called `r_pois_reg` which implements a Bayesian regression model.

<CodeBlock language="py" title="models.py">{PythonModels}</CodeBlock>
<CodeBlock language="r" title="models.R">{RModels}</CodeBlock>

### Deployment architecture

The project uses git and GitLab for source code management. The project operates two principal branches, 'dev' and 'main'. The 'main' branch is a protected branch and is only committed to with the intention of releasing a new version of the package. The 'dev' branch is the principal development branch. Any subsidiary branches must first be merged into 'dev' before being merged into 'main'.

GitLab CI/CD is used for 

* testing
* building/deploying the Python package
* building/deploying the documentation site

Commits to the 'dev' branch trigger a job which runs all tests excluding the functional tests.

Commits to the 'main' branch trigger a pipeline which 

* runs all tests
* builds/deploys the Python package
* builds/deploys the documentation site

All CI/CD stages run in a Docker container. This project uses `node:latest` to build and deploy the documentation and a custom R/Python container specified by a Dockerfile for the remaining jobs.

MVF uses [Docusaurus](https://docusaurus.io) to create documentation. The site is deployed to a public URL via GitLab Pages.

![image](/static/img/deployment_architecture.png)

### Testing

Multiple levels of testing

* unit
* integration
* functional
* system/end-to-end
