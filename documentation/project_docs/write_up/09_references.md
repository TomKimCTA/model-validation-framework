# References

Generated using https://www.mybib.com/tools/harvard-referencing-generator

# 1

Fienberg, S.E. (2006). When did Bayesian inference become ‘Bayesian’?. Bayesian Analysis, 1(1), pp.1–40. doi:https://doi.org/10.1214/06-ba101.

# 2

Fradkov, A.L. (2020). Early History of Machine Learning. IFAC-PapersOnLine, 53(2), pp.1385–1390. doi:https://doi.org/10.1016/j.ifacol.2020.12.1888.

# 3

Sculley, D., Holt, G., Golovin, D., Davydov, E., Phillips, T., Ebner, D., Chaudhary, V., Young, M., Crespo, J.-F. and Dennison, D. (2015). Hidden Technical Debt in Machine Learning Systems. MIT Press, pp.2503–2511.

# 4

Kreuzberger, D., Kühl, N. and Hirschl, S. (2023). Machine Learning Operations (MLOps): Overview, Definition, and Architecture. IEEE Access, 11, pp.31866–31879. doi:https://doi.org/10.1109/access.2023.3262138.

# 5

Fowler, M. (2019). Continuous Delivery for Machine Learning. [online] martinfowler.com. Available at: https://martinfowler.com/articles/cd4ml.html [Accessed 16 Jun. 2023].


## hidden technical debt [3]:

unstable data dependencies - evolving input features

under-utilised data dependencies - redundant features in a model can have long-term consequences (e.g. two features are correlated, one causal, one not, model credits non-causal feature, correlation changes over time, model is brittle.) - fixed by leave-one-out feature evaluations. 

glue code - the code required to support the adoption of generic ML package. Wrap generic packages with a common API.

'If data replaces code in ML systems, and code should be tested, then it seems clear that some amount of testing of input data is critical to a well-functioning system. Basic sanity checks are useful, as more sophisticated tests that monitor changes in input distributions.'


## cd4ml

Martin Fowler [article](https://martinfowler.com/articles/cd4ml.html)

* Need to version in three 'axes'
    * data
        * schema
        * sampling over time
        * volume
    * model
        * algorithms
        * more training
        * experiments
    * code
        * business needs (e.g. increased performance)
        * bug fixes
        * configuration

See [slides](https://docs.google.com/presentation/d/1Vvfc3UK8mkaWy4Ybg4_YDUs3oqb9YcogERe1bnF9-cE/edit#slide=id.p) for presentation given to team.

## Metrics for uncertainty evaluation in regression problems

[article](https://towardsdatascience.com/metrics-for-uncertainty-evaluation-in-regression-problems-210821761aa).

algorithms
* LightGBM with Quantile Regression
* NGBoost — a gradient boosting algorithm with probabilistic prediction
* Probabilistic Regression Neural Network that models mean and standard deviation

metrics:

* Validity— evaluation of the reliability of quantiles and bias in a probabilistic context
* Sharpness — estimating concentration of probabilities (prediction intervals)
* Negative Log-Likelihood (NLL) — the likelihood for the observed data to occur given the inferred parameters of the conditional distribution
* Continuous Ranked Probability Score (CRPS) — estimating how close the conditional distribution is to the observed point

## Other frameworks

[MLFlow](https://mlflow.org/docs/latest/what-is-mlflow.html)

#### Pros

* Extremely flexible
* Supports R and Python
* Nice UI
* Supports many of the iterative CD features (e.g. ratcheting) that we are after

#### Cons

* Users would need to learn several APIs to use effectively
* We are currently interested in a specific regression use case. Implementing this workflow from scratch would be time consuming
* According to competitors, not good jupyter notebook experience. Especially for results exploration - a key component of MVF.
* Very geared towards pure performance rather than uncertainty quantification. However, fairly sure this is configurable.

#### Summary

Highly likely that this will be the tool we use for managing production models. But maybe not best suited for interacting with collaborators.

[MLFlow Recipes](https://mlflow.org/docs/latest/recipes.html) is almost exactly what we want. But does not allow for integration with R.

Suggest we use MVF for selecting candidate models (in collaboration with academics etc.) but use MLFlow for managing the ML lifecycle of selected models. There should be a focus then on keeping MVF lightweight and not over investing.