# Aims & Objectives

## Key features

* Pluggable
    * model
    * data set
* Supports R and Python
* Minimal user code
* Pipeline testing
* Must be flexible enough to accommodate any new model
