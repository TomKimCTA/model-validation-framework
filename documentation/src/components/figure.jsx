import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";

export default function Figure({ src, caption }) {
  return (
    <figure>
      <img src={useBaseUrl(src)} alt={caption} />
      <figcaption style={{ textAlign: "right", fontStyle: "italic" }}>
        {caption}
      </figcaption>
    </figure>
  );
}
