---
sidebar_position: 2
---
# Splitting data

A guide to the split options available with MVF.

### Train/test

Use `split: train_test` to split the data in two. The proportion of instances allocated to the test set is determined by the `test_size` parameter.

### K-fold

Use `split: k_fold` to split the data into `n_fold: k` equal folds. This approach can reduce the variance component of error.

### Grouping variables

Use `grouping_variable: column_name` to specify a variable to group on when splitting data. This option is available for both `train_test` and `k_fold` splits.

### Temporal train/test split with grouping

The development of MVF was motivated by work on a dataset comprised of hierarchical time series. In other words, the data includes a grouping variable and a temporal variable. Use of the `data.grouping_variable` parameter and `data.temporal_split` invokes the following methodology:

* Perform train/test split with the grouping variable.
* For each target feature time series in the test set (each instance of the grouping variable), split the time series according to a proportion determined by `data.temporal_split.test_size`.
* The latter portion of the target feature series is used for validation. 
* The former portion is put back into the training set.

![image](/img/temp_split.png)

Metrics are calculated on non-null values of the validation instance.

:::caution
Support for K-fold temporal splits is yet to be implemented.
:::

### Leave-one-out/leave-n-out

Leave-one-out/leave-n-out validation is not currently supported in MVF. It is possible to implement a workaround using K-fold validation, making K large enough.


