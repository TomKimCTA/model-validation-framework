---
sidebar_position: 3
---

# User Guide

## Project configuration

* [Preprocessing data](preprocessing_data)
* [Splitting data](splitting_data)
* [Writing the model class](writing_the_model_class)
* [R models](r_models)
* [Model specific validation](model_specific_validation)
* [Model uncertainty](model_uncertainty)
* [Feature scaling](feature_scaling)

## Project outputs

* [Outputs](outputs)
* [Validation](validation)