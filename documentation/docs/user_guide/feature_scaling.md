---
sidebar_position: 14
---

# Feature scaling

Features should be scaled within the model class. The scaler should be saved with the model.

:::tip
Try out an [example](/examples#feature-scaling).
:::

```py title="Python standardisation example"
# mvf imports
import pandas
# user imports
import pickle
from scipy import stats
from sklearn.linear_model import BayesianRidge
from sklearn.preprocessing import StandardScaler


class bayes_reg_scaled:
    def __init__(self):
        '''
        Initialise the model and scaler.
        '''
        self.model = BayesianRidge()
        self.scaler = StandardScaler()

    def fit(self, X, y):
        '''
        Fit model to scaled data.
        '''
        self.scaler.fit(X)
        self.model.fit(
            self.scaler.transform(X),
            y
        )

    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred = self.model.predict(
            self.scaler.transform(X)
        )
        preds = pandas.DataFrame()
        preds['target feature'] = mean_pred
        return preds

    def save(self, path):
        '''
        Save model and scaler.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)
        with open(f'{path}_scaler', 'wb') as f:
            pickle.dump(self.scaler, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model and scaler.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)
        with open(f'{path}_scaler', 'rb') as f:
            self.scaler = pickle.load(f)
```