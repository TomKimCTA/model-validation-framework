---
sidebar_position: 1
---
# Preprocessing data

The code must take the form of a Jupyter notebook (.ipynb), but may be written in Python or R. In the [project configuration](/documentation/config_api), the language used is specified by the `lang` parameter and the location of the code is specified by the `source` parameter.

## What should this process do?

Write code to fetch and preprocess data in this template. This should include encoding categorical features.

:::caution
Feature scaling should not be done in this template. See the [guide](/user_guide/feature_scaling).
:::

Ensure you split data into 2 objects:

* **X** - input features
* **y** - target features

These objects must be pandas.DataFrame or R data.frame. The **X** data frame columns must contain the `input_features` specified in `mvf_conf.yaml`. The **y** data frame must contain the `target_features` specified in `mvf_conf.yaml`.