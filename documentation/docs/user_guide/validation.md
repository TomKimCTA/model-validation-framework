---
sidebar_position: 20
---
# Validation

MVF provides several validation stages out of the box

* [Error metrics](#error-metrics)
* [Uncertainty quantification](#uncertainty-quantification)
* [Computational performance](#computational-performance)

Furthermore, the final validation step is saved as an executed notebook using [Papermill](https://papermill.readthedocs.io/en/latest/). This allows a user to implement their own validation measures after execution.

:::info
The validation notebook only supports Python. 
:::

## Error metrics

Generic regression performance metrics

* MSE
* RMSE
* MAE
* MAPE
* R-Squared (coefficient of determination)

## Uncertainty quantification

:::info
This implementation has been designed for Bayesian credible intervals.
:::

### Quantile interval coverage

For each of the intervals defined in `quantile_intervals`, what proportion of actual values fall within the associated interval?

:::info
For a 95% CI, we would hope that approximately 95% of the actual values fall within their associated interval.
:::

### Model sharpness

How uncertain is the model? `sharpness` of a quantile interval is defined as the average width of the interval across all predictions.

## Computational performance

Computational performance metrics

- fit time
- predict time
- model size

## Plots

Some models benefit from qualitative validation from plots. The plotting utility currently supports

* hierarchical time series