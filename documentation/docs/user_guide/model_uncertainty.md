---
sidebar_position: 10
---
# Model uncertainty

Certain models are capable of reporting uncertainty with respect to their predictions. This uncertainty can then be quantified into [quantile interval coverage](/user_guide/validation#quantile-interval-coverage) and [model sharpness](/user_guide/validation#model-sharpness).

:::info
This implementation has been designed for Bayesian credible intervals.
:::

## Configuration

Specify the intervals relevant to your project.

```yaml title="mvf_conf.yaml"
output:
  quantile_intervals: [
    [0.01, 0.99],
    [0.025, 0.975],
    [0.05, 0.95],
    [0.1, 0.9]
  ]
```

Specify which models will return quantile estimates using the `return_quantiles` parameter.

```yaml title="mvf_conf.yaml"
models:
  - name: py_bayes_reg
    ...
    return_quantiles: true

  - name: r_pois_reg
    ...
    return_quantiles: false
```

## Writing the model class

If a model has `return_quantiles: true`, the *predict()* method must return the appropriate quantiles as columns in the returned dataframe.

For example, if the quantiles intervals and target featuress are specified as

```yaml title="mvf_conf.yaml
data:
  ...
  target_features:
    - y1
    - y2
output:
  quantile_intervals: [
    [0.1, 0.95],
    [0.1, 0.9]
  ]
```

The columns of the returned dataframe should be

* `y1`
* `y1_Q10`
* `y1_Q90`
* `y1_Q95`
* `y2`
* `y2_Q10`
* `y2_Q90`
* `y2_Q95`

:::tip
In Python, convert the `target` feature and `decimal_quantile` to a column header using an f-string

```py
f'{target}_Q{decimal_quantile * 100:g}'
```
:::