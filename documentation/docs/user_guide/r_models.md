---
sidebar_position: 4
---
# R models

A guide for writing model implementations in R. In the example below, we will implement a Poisson regression model in R.

## Configuration

Ensure the model specification in `mvf_conf.yaml` has the `lang: R` parameter set.

```yaml title="mvf_conf.yaml"
models:
  - name: r_pois_reg
    lang: R
```

## Writing the model class

To write an OOP class in R, we use the `R6` library. For a full guide for writing model classes, see the [guide](/user_guide/writing_the_model_class). For a simple example implementing a Poisson regression, see below:

```r title="models.R"
# mvf imports
library(R6)

r_pois_reg <- R6Class(
    'r_pois_reg',
    public = list(
        model = NULL,
        fit = function(X, y){
            data <- cbind(X,y)
            self$model <- glm(
                count ~ year + month + hour + workingday + weather + temp + feel_temp + humidity + windspeed,
                data=data,
                family=poisson(),
            )
            print('r_pois_reg fit')
        },
        predict = function(X, quantile_intervals){
            # must format data into a dataframe
            preds <- predict(
                self$model, 
                newdata=X
            )
            data.frame(predictions = preds)
        },
        save = function(path){
            saveRDS(self$model, path)
        },
        load = function(path){
            self$model <- readRDS(path)
        }
    )
)
```