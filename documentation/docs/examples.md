---
sidebar_position: 4
---

# Examples

## Python preprocessing with train/test split

Download the example using

```bash
mvf examples -n py_preprocess_train_test
```

Run the project using `mvf run`.

## R preprocessing with K-fold cross-validation

Download the example using

```bash
mvf examples -n r_preprocess_kfold
```

Run the project using `mvf run`.

## Feature scaling

Download the example using

```bash
mvf examples -n feature_scaling
```

Run the project using `mvf run`.
