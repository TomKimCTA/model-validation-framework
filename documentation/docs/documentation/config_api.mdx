---
sidebar_position: 1
toc_max_heading_level: 4
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Configuration API

This section documents the schema for the `mvf_conf.yaml` configuration file.

## data

Schema for the configuration of the project's dataset.

### data.source

Path to the data preprocessing source code. This may be a relative path from the project directory. The source code must take the form of a Python or R notebook (`ipynb`), configured by the `data.lang` parameter. The code should contain a cell tagged `parameters`:

<Tabs>
<TabItem value="python" label="Python">

```python
# appropriate parameters are injected here at run time
upstream = None
product = None
input_features = []
target_features = []
```

</TabItem>
<TabItem value="r" label="R">

```r
# appropriate parameters are injected here at run time
upstream = NULL
product = NULL
input_features = list()
target_features = list()
```


</TabItem>
</Tabs>

The notebook must also split the dataset into input features, `X`, and target features, `y`. These data frames must then be saved to disk:

<Tabs>
<TabItem value="python" label="Python">

```python
import feather
# save data for next process
feather.write_dataframe(X, product['X_data'])
feather.write_dataframe(y, product['y_data'])
```

</TabItem>
<TabItem value="r" label="R">

```r
require(arrow)
# save data for next process
write_feather(X, product[['X_data']])
write_feather(y, product[['y_data']])
```

</TabItem>
</Tabs>

### data.lang

The language of the preprocessing code:

* `Python`
* `R`

### data.input_features

A list of column labels specifying the input features.

### data.target_features

A list of column labels specifying the target features.

### data.split

Specifies how to split the data:

* `train_test` - splits the data into a train set and test set.
* `k_fold` - splits the data into k equal folds and train k models, whereby the k-th model is trained on all the data except the k-th fold.

If `train_test` is given, the [`data.test_size`](#datatest_size) parameter must be specified.

If `k_fold` is given, the [`data.n_folds`](#datan_folds) parameters must be specified.

### data.test_size

Specifies what proportion of data to use for validation. Must be a numeric value between 0 and 1. Must be used in conjunction with `data.split: train_test`.

### data.n_folds

Specifies how many folds to split the data into. Must be a positive integer greater than or equal to 2. Must be used in conjunction with `data.split: train_test`.

### data.grouping_variable

Specifies the label of a variable to group on when splitting the data.

### data.temporal_split

Specifies whether to perform an additional temporal split on the test data. This set of parameters only works in the case where a `grouping_variable` is used.

#### data.temporal_split.temporal_variable

The variable representing time. Must be an ordinal type.

#### data.temporal_split.test_size

The proportion of the time series to withhold for validation. The proportion is calulated between the first and last non-null value of the time series.

* *float* **or** *list* of two *float* values
    - If *float*, value between 0 and 1. Each time series will be split according to this proportion.
    - If *list*, length must be 2 and each value, **x** and **y**, must be *float* between 0 and 1. Each time series will be split according to a proportion **u** ~ Unif(**x**, **y**).

```yaml title="mvf_conf.yaml"
data:
  ...
  temporal_split:
    temporal_variable: date
    test_size: 0.3
    # or
    test_size: [0.2, 0.8]
```

:::info
See the [guide](/user_guide/splitting_data#temporal-traintest-split-with-grouping) for a full explanation.
:::

## output

Schema for the configuration of project outputs.

### output.quantile_intervals

Specifies the quantile intervals on which to calculate coverage/sharpness. Must be a list of lists. Each nested list must contain two numeric values between 0 and 1.

```yaml title='mvf_conf.yaml'
output:
  quantile_intervals:
    [
      [0.025, 0.975],
      [0.05, 0.975]
    ]
```

## models

Schema for the configuration of project models.

### models\[*\].name

The name of the model class. This must match a class defined in `models.py` or `models.R`.

### models\[*\].lang

The language that the model code uses:

* `Python` - the model class must be found in `models.py`
* `R` - the model class must be found in `models.R`

### models\[*\].validation_step

Specifies whether the model has a specific validation step. E.g. checking convergence of MCMC. *Boolean*. If `true`, requires the model class to have a `validate()` method.

### models\[*\].return_quantiles

Specifies whether the model is expected to report its uncertainty. *Boolean*. If true, the `predict()` method must return the quantiles specified in `quantile_intervals`. For example, with the following configuration:

```yaml title='mvf_conf.yaml'
...
output:
  quantile_intervals:
    [
      [0.025, 0.975],
      [0.05, 0.975]
    ]

models:
  model_1:
    ...
    return_quantiles: true
```

The `model_1.predict()` method must return a dataframe including the following columns:

* `predictions`
* `Q2.5`
* `Q5`
* `Q97.5`