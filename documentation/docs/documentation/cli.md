---
sidebar_position: 2
---
# Command line interface

This section documents the available shell commands. To see details in the command line, use `mvf --help` or `mvf command_name --help`

## `init`

```sh
mvf init
```

Creates a new MVF project in the working directory (generates a template `mvf_conf.yaml`). Asks for confirmation before proceeding. Will not overwrite an existing configuration file.

### `directory`

```sh
mvf init -d path_to_my_project_dir
```

Creates a new directory or uses existing directory at the path given in the argument (relative or absolute). Generates a template `mvf_conf.yaml` in the directory.

## `scaffold`

```sh
mvf scaffold
```

Parses `mvf_conf.yaml` and generates template project files

* A template for the data source code (.ipynb)
* Template `models.py`
* Template `models.R`

:::danger
The command will overwrite existing files.
:::

## `run`

```sh
mvf run
```

Runs the project. Incremental execution by default (MVF will skip processes that have already been executed). `run` also plots the workflow as for [`plot`](#plot).

### `force`

```sh
mvf run -f
```

Executes the workflow from the start, regardless of the status of any processes.

### `output`

```sh
mvf run -o custom_directory
```

Outputs are saved to `custom_directory/` rather than `output/`.

:::tip
Save outputs to a datestamped directory using

```sh
mvf run -o "output_$(date +'%Y_%m_%d')"
```
:::

### `process`

```sh
mvf run -p process_name
```

Executes only the process name `process_name`.

:::tip
Find a process name by looking at the [project workflow](/user_guide/outputs#project-workflow).
:::

## `plot`

```sh
mvf plot
```

Plots the project workflow. Saves the plot as `project_workflow.html` in the project directory.

## `examples`

### `list`

```sh
mvf examples -l
```

List the available examples.

### `name`

```sh
mvf examples -n example_name
```

Download the example called `example_name`.

### `output`

```sh
mvf examples -n example_name -o target_directory
```

Download the example called `example_name` into a directory called `target_directory/`.