# Getting Started

#### Prerequisites

- Python 3.x
  - [PyPI](https://pip.pypa.io/en/stable/installation/)
- R 4.x
  - [IRkernel](https://github.com/IRkernel/IRkernel)
  - [R6](https://r6.r-lib.org/articles/Introduction.html)
  - [arrow](https://arrow.apache.org/docs/r/)

#### Installation

```bash
pip install mvf
```
