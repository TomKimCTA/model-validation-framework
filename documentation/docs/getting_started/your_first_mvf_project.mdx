---
sidebar_position: 2
---

import Tabs from "@theme/Tabs";
import TabItem from "@theme/TabItem";

# Your first MVF project

Follow this walkthrough for an overview of MVF.

## Initialise your project

Suppose your project is called `first_mvf_project`. Run

```bash
mvf init -d first_mvf_project
```

This creates a new directory called `first_mvf_project` in the working directory. That directory will contain a template `mvf_conf.yaml`.

```yaml title="Template configuration file"
data:
  source: path_to_your_source_code
  lang: Python
  split: train_test
  test_size: 0.3
  input_features: []
  target_features: []
models:
  - lang: Python
    name: your_model_name
```

### MVF configuration

We need to define the

- source for the data
- first model in our project

We will write the code to preprocess our data in a notebook called `preprocess_data.ipynb`. This can be a Python or R notebook (specified by [`data.lang`](/documentation/config_api#datalang)). The first model we will add will be a Python implemented Bayesian ridge regression.

:::info
For R model implementations, see the [User Guide - R models](/user_guide/r_models).
:::

Edit the configuration file with the following values:

```yaml title="mvf_conf.yaml"
data:
  source: preprocess_data.ipynb
  lang: R    # if R preferred over Python
  ...

models:
- name: py_bayes_reg
  lang: Python
```

## Scaffold your project

Run

```bash
mvf scaffold
```

:::caution
Ensure you have navigated to the project directory with `cd first_mvf_project`.
:::

This command will generate template files based on `mvf_conf.yaml`. In this case

- `preprocess_data.ipynb`
- `models.py`

### Write preprocessing code

We need to modify `preprocess_data.ipynb` such that the code will

- extract data (from an API or from disk - e.g. csv)
- perform any preprocessing
- divide the data into
  - **X** - input features
  - **y** - target features
- save the data for the next process

In this example, we use a dataset containing information about hourly usage of a bike sharing scheme. The input features include data about

- the hourly window
  - hour of the day
  - month and season
  - weekday/weekend
  - working day/holiday
- weather conditions
  - adjective (e.g. clear, misty)
  - temperature/temperature feel
  - humidity
  - windspeed

We will define the input and target features in the configuration file first, using the `input_features` and `target_features` parameters:

```yaml title="mvf_conf.yaml"
data:
  ...
  input_features:
    - year
    - month
    - hour
    - workingday
    - weather
    - temp
    - feel_temp
    - humidity
    - windspeed
  target_features:
    - count
  ...
```

We can then use those parameters in our preprocessing code to subset the original dataset.

Add the following in place of the markdown cell starting `Write code to fetch...`:

<Tabs>
<TabItem value="py" label="Python">

```py
# import API to fetch dataset
from sklearn.datasets import fetch_openml
# fetch data
bike_sharing = fetch_openml(
    "Bike_Sharing_Demand",
    version=2,
    as_frame=True,
    parser='auto'
)
# divide data into input and target features
X = bike_sharing.data[
    input_features
]
y = pandas.DataFrame(bike_sharing.target, columns=target_features)
# encode weather and workingday as ordinal int
X['weather'].replace(
    {
        'clear': 0,
        'misty': 1,
        'rain': 2,
        'heavy_rain': 3
    },
    inplace=True
)
X['workingday'].replace(
    {
        'False': 0,
        'True': 1
    },
    inplace=True
)
# Change column types to integer.
X['weather'] = X['weather'].astype('int64')
X['workingday'] = X['workingday'].astype('int64')
```

</TabItem>
<TabItem value="r" label="R">

```r
# load data from disk
bike_sharing = read.csv('bike_sharing.csv')
# divide data into input and target features
X = bike_sharing[
    unlist(input_features)
]
y = bike_sharing[unlist(target_features)[1]]
# Encode weather and workingday as ordinal integer
weather_encoder = c(0, 1, 2, 3)
names(weather_encoder) = c('clear', 'misty', 'rain', 'heavy_rain')
X$weather = weather_encoder[X$weather]
workingday_encoder = c(0, 1)
names(workingday_encoder) = c('False', 'True')
X$workingday = workingday_encoder[X$workingday]
```

</TabItem>
</Tabs>

:::info
R users can download the dataset [here](/resources/bike_sharing.csv). Save the CSV to the project directory as `bike_sharing.csv`.
:::

The template will already have a cell to save the data

<Tabs>
<TabItem value="py" label="Python">

```py
# save data for next process
feather.write_dataframe(X, product['X_data'])
feather.write_dataframe(y, product['y_data'])
```

</TabItem>
<TabItem value="r" label="R">

```r
# save data for next process
write_feather(X, product[['X_data']])
write_feather(y, product[['y_data']])
```

</TabItem>
</Tabs>

### Write model classes

We need to implement the class that has been templated in `models.py`.

```py title="models.py"
# python models file

# mvf imports
import pandas
# user imports
import pickle
from scipy import stats
from sklearn.linear_model import BayesianRidge

class py_bayes_reg:
    def __init__(self):
        '''
        Initialise the model object with any hyperparameters
        '''
        self.model = BayesianRidge()

    def fit(self, X, y):
        '''
        Fit model to data.
        '''
        self.model.fit(X, y)

    def predict(self, X, quantiles=None) -> pandas.DataFrame:
        '''
        Predict target values (y) from new data.
        '''
        mean_pred = self.model.predict(
            X
        )
        preds = pandas.DataFrame()
        preds['count'] = mean_pred
        return preds

    def save(self, path):
        '''
        Save model.
        '''
        with open(path, 'wb') as f:
            pickle.dump(self.model, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        '''
        Load a model.
        '''
        with open(path, 'rb') as f:
            self.model = pickle.load(f)
```

## Run your project

Use `mvf run` to run the project. Check the results:

* Visualise the project workflow by looking at `project_workflow.html`.
* See the validation summary by looking at `output/notebooks/validate.html`.

Customise the validation by editing and re-executing `output/notebooks/validate.ipynb`.

:::info
It's best to move `validate.ipynb` to the project directory before re-executing. This ensures any edits are not overwritten by a subsequent run of the project and that the file system navigation is consistent.
:::

Check the archive assets:

* Intermediate data is stored under `output/data/`.
* The fit model is stored under `output/models/`.

## Next steps

Add another model to `mvf_conf.yaml`. See the [User Guide](/user_guide) for more details.