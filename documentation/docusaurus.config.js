const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const config = {
  title: 'Model Validation Framework',
  url: 'https://tomkimcta.gitlab.io',
  organizationName: 'TomKimCTA',
  projectName: 'model-validation-framework',
  
  baseUrl: '/model-validation-framework/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',

  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      ({
        docs: {
          routeBasePath: '/',
        },
       blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    ({
      navbar: {
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Model Validation Framework',
          },
          {
            href: 'https://gitlab.com/TomKimCTA/model-validation-framework',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Introduction',
                to: '/',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'GitLab',
                href: 'https://gitlab.com/TomKimCTA/model-validation-framework',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Certus Technology Associates, built with Docusaurus.`,
      },
      prism: {
        additionalLanguages: ['r'],
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],
};

module.exports = config;
